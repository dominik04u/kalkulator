package com.example.hp.kalkulator;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


public class advancedFragment extends Fragment implements View.OnClickListener {
    private static TextView textView;
    View view;
    Button but0,but1,but2,but3,but4,but5,but6,but7,but8,but9,butKropka,butRowne,butPlus,butMinus,butMnoz,butDziel, butC,butPM,butBack,butSqrt,butSin,butCos,butTan,butLn,butx2,butxy,butLog;
    String text1="",text2="",znak="";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.advanced, container, false);
        textView = ((TextView) view.findViewById(R.id.textAdvanced));

        but0 = (Button) view.findViewById(R.id.advanced0Button);
        but0.setOnClickListener(this);
        but1 = (Button) view.findViewById(R.id.advanced1Button);
        but1.setOnClickListener(this);
        but2 = (Button) view.findViewById(R.id.advanced2Button);
        but2.setOnClickListener(this);
        but3 = (Button) view.findViewById(R.id.advanced3Button);
        but3.setOnClickListener(this);
        but4 = (Button) view.findViewById(R.id.advanced4Button);
        but4.setOnClickListener(this);
        but5 = (Button) view.findViewById(R.id.advanced5Button);
        but5.setOnClickListener(this);
        but6 = (Button) view.findViewById(R.id.advanced6Button);
        but6.setOnClickListener(this);
        but7 = (Button) view.findViewById(R.id.advanced7Button);
        but7.setOnClickListener(this);
        but8 = (Button) view.findViewById(R.id.advanced8Button);
        but8.setOnClickListener(this);
        but9 = (Button) view.findViewById(R.id.advanced9Button);
        but9.setOnClickListener(this);
        butKropka = (Button) view.findViewById(R.id.advancedDotButton);
        butKropka.setOnClickListener(this);
        butRowne = (Button) view.findViewById(R.id.advancedEqualsButton);
        butRowne.setOnClickListener(this);
        butPlus = (Button) view.findViewById(R.id.advancedPlusButton);
        butPlus.setOnClickListener(this);
        butMinus = (Button) view.findViewById(R.id.advancedMinusButton);
        butMinus.setOnClickListener(this);
        butMnoz = (Button) view.findViewById(R.id.advancedD2Button);
        butMnoz.setOnClickListener(this);
        butDziel = (Button) view.findViewById(R.id.advancedD1Button);
        butDziel.setOnClickListener(this);
        butC = (Button)view.findViewById(R.id.advancedCancelButton);
        butC.setOnClickListener(this);
        butPM = (Button)view.findViewById(R.id.advancedPMButton);
        butPM.setOnClickListener(this);
        butBack = (Button)view.findViewById(R.id.advancedBackspaceButton);
        butBack.setOnClickListener(this);
        butSqrt = (Button) view.findViewById(R.id.advancedSqrtButton);
        butSqrt.setOnClickListener(this);
        butSin = (Button) view.findViewById(R.id.advancedSinButton);
        butSin.setOnClickListener(this);
        butCos = (Button) view.findViewById(R.id.advancedCosButton);
        butCos.setOnClickListener(this);
        butTan = (Button) view.findViewById(R.id.advancedTanButton);
        butTan.setOnClickListener(this);
        butLn = (Button) view.findViewById(R.id.advancedLnButton);
        butLn.setOnClickListener(this);
        butx2 = (Button) view.findViewById(R.id.advancedPower2Button);
        butx2.setOnClickListener(this);
        butxy = (Button) view.findViewById(R.id.advancedPowerYButton);
        butxy.setOnClickListener(this);
        butLog = (Button) view.findViewById(R.id.advancedLogButton);
        butLog.setOnClickListener(this);
        return view;
    }

    private void dodawanie(){
        float l1,l2,wynik;
        if(text1.equals(""))
            l1=0;
        else
            l1=Float.parseFloat(text1);
        if(text2.equals(""))
            l2=0;
        else
            l2=Float.parseFloat(text1);
        wynik=l1+l2;
        text1=String.valueOf(wynik);
    }

    private void odejmowanie(){
        float l1,l2,wynik;
        if(text1.equals(""))
            l1=0;
        else
            l1=Float.parseFloat(text2);
        if(text2.equals(""))
            l2=0;
        else
            l2=Float.parseFloat(text1);
        wynik=l1-l2;
        text1=String.valueOf(wynik);
    }

    private void mnozenie(){
        float l1,l2,wynik;
        if(text1.equals(""))
            l1=0;
        else
            l1=Float.parseFloat(text2);
        if(text2.equals(""))
            l2=0;
        else
            l2=Float.parseFloat(text1);
        wynik=l1*l2;
        text1=String.valueOf(wynik);
    }

    private void dzielenie(){
        float l1,l2,wynik;
        if(text1.equals(""))
            l1=0;
        else
            l1=Float.parseFloat(text2);
        if(text2.equals(""))
            l2=0;
        else
            l2=Float.parseFloat(text1);
        wynik=l1/l2;
        text1=String.valueOf(wynik);
    }

    private void backspace(){
        if(text1.length()>0)
            text1=text1.substring(0,text1.length()-1);
    }

    private void zmianaZnaku(){
        Float zm;
        if((text1!=""&&text1!="+")&&(text1!="-")&&(text1!="*")&&(text1!="/")){
            zm=Float.parseFloat(text1);
            zm*=-1;
            text1=String.valueOf(zm);
        }
    }

    private void pierwiastek(){
        float l,wynik;
        if(text1.equals(""))
            l=0;
        else
            l=Float.parseFloat(text1);
        text1=String.valueOf(Math.sqrt(l));
    }

    private void potega2(){
        float l;
        if(text1.equals(""))
            l=0;
        else
            l=Float.parseFloat(text1);
        text1=String.valueOf(Math.pow(l,2));
    }

    private void potegaY(){
        float l1,l2;
        if(text1.equals(""))
            l1=0;
        else
            l1=Float.parseFloat(text1);
        if(text2.equals(""))
            l2=0;
        else
            l2=Float.parseFloat(text2);
        text1=String.valueOf(Math.pow(l2,l1));
    }

    private void log(){
        float l;
        if(text1.equals(""))
            l=0;
        else
            l=Float.parseFloat(text1);
        text1=String.valueOf(Math.log10(l));
    }

    private void logN(){
        float l;
        if(text1.equals(""))
            l=0;
        else
            l=Float.parseFloat(text1);
        text1=String.valueOf(Math.log(l));
    }

    private void sinus(){
        float l;
        if(text1.equals(""))
            l=0;
        else
            l=Float.parseFloat(text1);
        text1=String.valueOf(Math.sin(Math.toRadians(l)));
    }

    private void cosinus(){
        float l;
        if(text1.equals(""))
            l=0;
        else
            l=Float.parseFloat(text1);
        text1=String.valueOf(Math.cos(Math.toRadians(l)));
    }

    private void tangens(){
        float l;
        if(text1.equals(""))
            l=0;
        else
            l=Float.parseFloat(text1);
        text1=String.valueOf(Math.tan(Math.toRadians(l)));
    }

    @Override
    public void onClick (View v){
        switch(v.getId()) {

            case R.id.advancedTanButton:
                tangens();
                textView.setText(text1);
                break;
            case R.id.advancedSinButton:
                sinus();
                textView.setText(text1);
                break;
            case R.id.advancedCosButton:
                cosinus();
                textView.setText(text1);
                break;
            case R.id.advancedSqrtButton:
                pierwiastek();
                textView.setText(text1);
                break;
            case R.id.advancedLnButton:
                logN();
                textView.setText(text1);
                break;
            case R.id.advancedLogButton:
                log();
                textView.setText(text1);
                break;
            case R.id.advancedPower2Button:
                potega2();
                textView.setText(text1);
                break;
            case R.id.advancedPowerYButton:
                text2=(String)textView.getText();
                text1="";
                znak="^";
                textView.setText(text1);
                break;
            case R.id.advancedBackspaceButton:
                backspace();
                textView.setText(text1);
                break;
            case R.id.advancedCancelButton:
                text1="";
                text2="";
                textView.setText(text1);
                break;
            case R.id.advancedPMButton:
                zmianaZnaku();
                textView.setText(text1);
                break;
            case R.id.advancedDotButton:
                if(text1.equals("")){
                    text1="0";
                }
                if(text1.contains(".")){}
                else
                    text1=text1+".";
                textView.setText(text1);
                break;
            case R.id.advancedD1Button:
                text2= (String) textView.getText();
                text1="";
                znak = "/";
                textView.setText(butDziel.getText());
                break;
            case R.id.advancedD2Button:
                text2= (String) textView.getText();
                text1="";
                znak = "*";
                textView.setText(butMnoz.getText());
                break;
            case R.id.advancedPlusButton:
                text2= (String) textView.getText();
                text1="";
                znak = "+";
                textView.setText(butDziel.getText());
                break;
            case R.id.advancedMinusButton:
                text2= (String) textView.getText();
                text1="";
                znak = "-";
                textView.setText(butDziel.getText());
                break;
            case R.id.advancedEqualsButton:
                switch (znak){
                    case "/":
                        dzielenie();
                        break;
                    case "*":
                        mnozenie();
                        break;
                    case "-":
                        odejmowanie();
                        break;
                    case "+":
                        dodawanie();
                        break;
                    case "^":
                        potegaY();
                        break;
                }
                znak="";
                textView.setText(text1);
                break;
            case R.id.advanced0Button:
                text1=text1+but0.getText();
                textView.setText(text1);
                break;
            case R.id.advanced1Button:
                text1=text1+but1.getText();
                textView.setText(text1);
                break;
            case R.id.advanced2Button:
                text1=text1+but2.getText();
                textView.setText(text1);
                break;
            case R.id.advanced3Button:
                text1=text1+but3.getText();
                textView.setText(text1);
                break;
            case R.id.advanced4Button:
                text1=text1+but4.getText();
                textView.setText(text1);
                break;
            case R.id.advanced5Button:
                text1=text1+but5.getText();
                textView.setText(text1);
                break;
            case R.id.advanced6Button:
                text1=text1+but6.getText();
                textView.setText(text1);
                break;
            case R.id.advanced7Button:
                text1=text1+but7.getText();
                textView.setText(text1);
                break;
            case R.id.advanced8Button:
                text1=text1+but8.getText();
                textView.setText(text1);
                break;
            case R.id.advanced9Button:
                text1=text1+but9.getText();
                textView.setText(text1);
                break;
            default:
                textView.setText("Error");
                break;

        }
    }
}
