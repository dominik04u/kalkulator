package com.example.hp.kalkulator;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;


public class Kalkulator extends Activity {

    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentManager=getFragmentManager();
        fragmentTransaction=fragmentManager.beginTransaction();
        fragmentTransaction.replace(android.R.id.content,new menuFragment());
        fragmentTransaction.commit();
    }

    public void buttonOnClick(View v){
        switch(v.getId()) {
            case R.id.SimpleButton:
                  fragmentTransaction = fragmentManager.beginTransaction();
                  fragmentTransaction.replace(android.R.id.content, new simpleFragment());
                  fragmentTransaction.addToBackStack(null);
                  fragmentTransaction.commit();
                  break;
            case R.id.AdvancedButton:
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(android.R.id.content, new advancedFragment());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;
            case R.id.AboutButton:
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(android.R.id.content, new aboutFragment());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;
            case R.id.ExitButton:
                finish();
                break;
        }
    }
}
