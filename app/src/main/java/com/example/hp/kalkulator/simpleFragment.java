package com.example.hp.kalkulator;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


public class simpleFragment extends Fragment implements View.OnClickListener {
    private static TextView textView;
    View view;
    Button but0,but1,but2,but3,but4,but5,but6,but7,but8,but9,butKropka,butRowne,butPlus,butMinus,butMnoz,butDziel, butC,butPM,butBack;
    String text1="",text2="",znak="";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.simple, container, false);
        textView = ((TextView) view.findViewById(R.id.textSimple));

        but0 = (Button) view.findViewById(R.id.simple0Button);
        but0.setOnClickListener(this);
        but1 = (Button) view.findViewById(R.id.simple1Button);
        but1.setOnClickListener(this);
        but2 = (Button) view.findViewById(R.id.simple2Button);
        but2.setOnClickListener(this);
        but3 = (Button) view.findViewById(R.id.simple3Button);
        but3.setOnClickListener(this);
        but4 = (Button) view.findViewById(R.id.simple4Button);
        but4.setOnClickListener(this);
        but5 = (Button) view.findViewById(R.id.simple5Button);
        but5.setOnClickListener(this);
        but6 = (Button) view.findViewById(R.id.simple6Button);
        but6.setOnClickListener(this);
        but7 = (Button) view.findViewById(R.id.simple7Button);
        but7.setOnClickListener(this);
        but8 = (Button) view.findViewById(R.id.simple8Button);
        but8.setOnClickListener(this);
        but9 = (Button) view.findViewById(R.id.simple9Button);
        but9.setOnClickListener(this);
        butKropka = (Button) view.findViewById(R.id.simpleDotButton);
        butKropka.setOnClickListener(this);
        butRowne = (Button) view.findViewById(R.id.simpleEqualsButton);
        butRowne.setOnClickListener(this);
        butPlus = (Button) view.findViewById(R.id.simplePlusButton);
        butPlus.setOnClickListener(this);
        butMinus = (Button) view.findViewById(R.id.simpleMinusButton);
        butMinus.setOnClickListener(this);
        butMnoz = (Button) view.findViewById(R.id.simpleD2Button);
        butMnoz.setOnClickListener(this);
        butDziel = (Button) view.findViewById(R.id.simpleD1Button);
        butDziel.setOnClickListener(this);
        butC = (Button)view.findViewById(R.id.simpleCancelButton);
        butC.setOnClickListener(this);
        butPM = (Button)view.findViewById(R.id.simplePMButton);
        butPM.setOnClickListener(this);
        butBack = (Button)view.findViewById(R.id.simpleBackspaceButton);
        butBack.setOnClickListener(this);
        return view;
    }

    private void dodawanie(){
        float l1,l2;
        if(text1.equals(""))
            l1=0;
        else
            l1=Float.parseFloat(text1);
        if(text2.equals(""))
            l2=0;
        else
            l2=Float.parseFloat(text2);
        text1=String.valueOf(l1+l2);
    }

    private void odejmowanie(){
        float l1,l2;
        if(text1.equals(""))
            l1=0;
        else
            l1=Float.parseFloat(text2);
        if(text2.equals(""))
            l2=0;
        else
            l2=Float.parseFloat(text1);
        text1=String.valueOf(l1-l2);
    }

    private void mnozenie(){
        float l1,l2;
        if(text1.equals(""))
            l1=0;
        else
            l1=Float.parseFloat(text2);
        if(text2.equals(""))
            l2=0;
        else
            l2=Float.parseFloat(text1);
        text1=String.valueOf(l1*l2);
    }

    private void dzielenie(){
        float l1,l2;
        if(text1.equals(""))
            l1=0;
        else
            l1=Float.parseFloat(text2);
        if(text2.equals(""))
            l2=0;
        else
            l2=Float.parseFloat(text1);
        text1=String.valueOf(l1/l2);
    }

    private void backspace(){
        if(text1.length()>0)
            text1=text1.substring(0,text1.length()-1);
    }

    private void zmianaZnaku(){
        Float zm;
        if((text1!=""&&text1!="+")&&(text1!="-")&&(text1!="*")&&(text1!="/")){
            zm=Float.parseFloat(text1);
            zm*=-1;
            text1=String.valueOf(zm);
        }
    }

    @Override
    public void onClick (View v){
        switch(v.getId()) {

            case R.id.simpleBackspaceButton:
                backspace();
                textView.setText(text1);
                break;
            case R.id.simpleCancelButton:
                text1="";
                text2="";
                textView.setText(text1);
                break;
            case R.id.simplePMButton:
                zmianaZnaku();
                textView.setText(text1);
                break;
            case R.id.simpleDotButton:
                if(text1.equals("")){
                    text1="0";
                }
                if(text1.contains(".")){}
                else
                    text1=text1+".";
                textView.setText(text1);
                break;
            case R.id.simpleD1Button:
                text2= (String) textView.getText();
                text1="";
                znak = "/";
                textView.setText(butDziel.getText());
                break;
            case R.id.simpleD2Button:
                text2= (String) textView.getText();
                text1="";
                znak = "*";
                textView.setText(butMnoz.getText());
                break;
            case R.id.simplePlusButton:
                text2= (String) textView.getText();
                text1="";
                znak = "+";
                textView.setText(butPlus.getText());
                break;
            case R.id.simpleMinusButton:
                text2= (String) textView.getText();
                text1="";
                znak = "-";
                textView.setText(butMinus.getText());
                break;
            case R.id.simpleEqualsButton:
                switch (znak){
                    case "/":
                        dzielenie();
                        break;
                    case "*":
                        mnozenie();
                        break;
                    case "-":
                        odejmowanie();
                        break;
                    case "+":
                        dodawanie();
                        break;
                }
                znak="";
                textView.setText(text1);
                break;
            case R.id.simple0Button:
                text1=text1+but0.getText();
                textView.setText(text1);
                break;
            case R.id.simple1Button:
                text1=text1+but1.getText();
                textView.setText(text1);
                break;
            case R.id.simple2Button:
                text1=text1+but2.getText();
                textView.setText(text1);
                break;
            case R.id.simple3Button:
                text1=text1+but3.getText();
                textView.setText(text1);
                break;
            case R.id.simple4Button:
                text1=text1+but4.getText();
                textView.setText(text1);
                break;
            case R.id.simple5Button:
                text1=text1+but5.getText();
                textView.setText(text1);
                break;
            case R.id.simple6Button:
                text1=text1+but6.getText();
                textView.setText(text1);
                break;
            case R.id.simple7Button:
                text1=text1+but7.getText();
                textView.setText(text1);
                break;
            case R.id.simple8Button:
                text1=text1+but8.getText();
                textView.setText(text1);
                break;
            case R.id.simple9Button:
                text1=text1+but9.getText();
                textView.setText(text1);
                break;
            default:
                textView.setText("Error");
                break;
        }
    }
}
